# folding-cell

**本项目是基于开源项目folding-cell进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/Ramotion/folding-cell-android ）追踪到原项目版本**

#### 项目介绍

- 项目名称：folding-cell折叠控件
- 所属系列：ohos的第三方组件适配移植
- 功能：提供一套自定义ComponentContainer，可支持子控件的折叠与展开效果
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/Ramotion/folding-cell-android
- 原项目基线版本：未发布Release版本
- 编程语言：Java
- 外部库依赖：无

#### 效果展示 

<img src="gif/folding_cell1.gif"/>

<img src="gif/folding_cell2.gif"/>

#### 安装教程

##### 方案一：

1. 下载folding-cell三方库har包library.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在module级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```groovy
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'folding_cell', ext: 'har')
	……
}
```

##### 方案二：

  1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/'
     }
 }
```

  2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'com.ramotion.foldingcell.ohos:folding-cell:1.0.0'
 }
```


#### 使用说明

1. 在Ability中使用自定义控件:

```xml
<com.ramotion.foldingcell.FoldingCell
    ohos:id="$+id:folding_cell"
    ohos:width="match_parent"
    ohos:height="wrap_content">
</com.ramotion.foldingcell.FoldingCell>
```

2. 在FoldingCell中添加两个子控件。第一个子控件(content view)代表了展开状态下显示的组件，第二个子控件(title view)则代表折叠状态下显示的组件。这两个子控件都可以进一步包含各自的层次结构，但需注意有如下约束: **content view 高度** 必须至少为 **title view 高度**的 **2倍** ，这两个布局的高度都必须设为 `ohos:height="match_content"`。如果你需要确切的 `vp`高度 , 你可以给 *content view* 或*title view*的子组件设置确切的高度。另外你需要通过属性`ohos:visibility="hide"`将*content view* 布局设置为不可见。


```xml
<com.ramotion.foldingcell.FoldingCell
    ohos:id="$+id:folding_cell"
    ohos:width="match_parent"
    ohos:height="match_content">

        <StackLayout
            ohos:id="$+id:cell_content_view"
            ohos:layout_width="match_parent"
            ohos:layout_height="match_content"
            ohos:background_element="#ff33ff33"
            ohos:visibility="hide">
            <Text
                ohos:width="match_parent"
                ohos:height="250vp" />
        </StackLayout>

        <StackLayout
            ohos:id="$+id:cell_title_view"
            ohos:width="match_parent"
            ohos:height="match_content">
            <Text
                ohos:width="match_parent"
                ohos:height="100vp"
                ohos:background_element="0xff3333ff" />
        </StackLayout>

</com.ramotion.foldingcell.FoldingCell>
```



3. 添加setClickedListener来触发 Folding Cell 组件的动画:


```java
@Override
protected void onStart(Intent intent) {
    super.onStart(intent);
    setUIContent(ResourceTable.Layout_activity_main);

    final FoldingCell fc = (FoldingCell) findComponentById(ResourceTable.Id_folding_cell);

    fc.setClickedListener(new Component.ClickedListener() {
        @Override
        public void onClick(Component c) {
            fc.toggle(false);
        }
    });
}
```


4. 额外属性说明：

   animationDuration：动画时长

   backSideColor：折叠动画播放时的背景色

   additionalFlipsCount：额外折叠层数

   cameraHeight：镜头深度

```xml
folding-cell:animationDuration="1000"
folding-cell:backSideColor="$color:bgBackSideColor"
folding-cell:additionalFlipsCount="2"
folding-cell:cameraHeight="30"
```

#### 版本迭代

- v1.0.0

  支持子控件折叠动画
  支持自定义属性调整动画效果
  受ohos接口限制，请采用DrawTask绘制子组件内容以获得更好效果

#### 版权和许可信息

```
- MIT License (MIT)
```
