package com.ramotion.foldingcell.examples.simple;

import com.ramotion.foldingcell.FoldingCell;
import com.ramotion.foldingcell.examples.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

public class SimpleAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_simple);

        // get our folding cell
        final FoldingCell fc = (FoldingCell) findComponentById(ResourceTable.Id_folding_cell);

        // attach click listener to fold btn
        final Button toggleBtn = (Button) findComponentById(ResourceTable.Id_toggle_btn);
        toggleBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                fc.toggle(false);
            }
        });

        // attach click listener to toast btn
        final Button toggleInstantlyBtn = (Button) findComponentById(ResourceTable.Id_toggle_instant_btn);
        toggleInstantlyBtn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                fc.toggle(true);
            }
        });
    }

}
