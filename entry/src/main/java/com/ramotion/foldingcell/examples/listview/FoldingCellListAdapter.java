package com.ramotion.foldingcell.examples.listview;

import com.ramotion.foldingcell.FoldingCell;

import java.util.HashSet;
import java.util.List;

import com.ramotion.foldingcell.examples.ResourceTable;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;
import ohos.utils.PlainArray;

/**
 * Simple example of ListAdapter for using with Folding Cell
 * Adapter holds indexes of unfolded elements for correct work with default reusable views behavior
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class FoldingCellListAdapter extends BaseItemProvider {

    private HashSet<Integer> unfoldedIndexes = new HashSet<>();
    private Component.ClickedListener defaultRequestBtnClickListener;
    private List<Item> objects;
    private Context context;
    private PlainArray<FoldingCell> cellCache = new PlainArray<>();

    public FoldingCellListAdapter(Context context, List<Item> objects) {
        this.objects = objects;
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer parent) {
        // get item for selected view
        Item item = getItem(position);
        // if cell is exists - reuse it, if not - create the new one from resource
        FoldingCell cell = cellCache.get(position, null);
        ViewHolder viewHolder;
        if (cell == null) {
            viewHolder = new ViewHolder();
            LayoutScatter vi = LayoutScatter.getInstance(getContext());
            cell = (FoldingCell) vi.parse(ResourceTable.Layout_cell, parent, false);
            cell.bindItemProvider(this);
            // binding view parts to view holder
            viewHolder.price = (Text) cell.findComponentById(ResourceTable.Id_title_price);
            viewHolder.time = (Text) cell.findComponentById(ResourceTable.Id_title_time_label);
            viewHolder.date = (Text) cell.findComponentById(ResourceTable.Id_title_date_label);
            viewHolder.fromAddress = (Text) cell.findComponentById(ResourceTable.Id_title_from_address);
            viewHolder.toAddress = (Text) cell.findComponentById(ResourceTable.Id_title_to_address);
            viewHolder.requestsCount = (Text) cell.findComponentById(ResourceTable.Id_title_requests_count);
            viewHolder.pledgePrice = (Text) cell.findComponentById(ResourceTable.Id_title_pledge);
            viewHolder.contentRequestBtn = (Text) cell.findComponentById(ResourceTable.Id_content_request_btn);
            cell.setTag(viewHolder);
            cellCache.append(position, cell);
        } else {
            // for existing cell set valid valid state(without animation)
            if (unfoldedIndexes.contains(position)) {
                cell.unfold(true);
            } else {
                cell.fold(true);
            }
            viewHolder = (ViewHolder) cell.getTag();
        }

        if (null == item)
            return cell;

        // bind data from selected element to view through view holder
        viewHolder.price.setText(item.getPrice());
        viewHolder.time.setText(item.getTime());
        viewHolder.date.setText(item.getDate());
        viewHolder.fromAddress.setText(item.getFromAddress());
        viewHolder.toAddress.setText(item.getToAddress());
        viewHolder.requestsCount.setText(String.valueOf(item.getRequestsCount()));
        viewHolder.pledgePrice.setText(item.getPledgePrice());

        // set custom btn handler for list item from that item
        if (item.getRequestBtnClickListener() != null) {
            viewHolder.contentRequestBtn.setClickedListener(item.getRequestBtnClickListener());
        } else {
            // (optionally) add "default" handler if no handler found in item
            viewHolder.contentRequestBtn.setClickedListener(defaultRequestBtnClickListener);
        }
        return cell;
    }

    // simple methods for register cell state changes
    public void registerToggle(int position) {
        if (unfoldedIndexes.contains(position))
            registerFold(position);
        else
            registerUnfold(position);
    }

    public void registerFold(int position) {
        unfoldedIndexes.remove(position);
    }

    public void registerUnfold(int position) {
        unfoldedIndexes.add(position);
    }

    public Component.ClickedListener getDefaultRequestBtnClickListener() {
        return defaultRequestBtnClickListener;
    }

    public void setDefaultRequestBtnClickListener(Component.ClickedListener defaultRequestBtnClickListener) {
        this.defaultRequestBtnClickListener = defaultRequestBtnClickListener;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Item getItem(int i) {
        return objects.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    // View lookup cache
    private static class ViewHolder {
        Text price;
        Text contentRequestBtn;
        Text pledgePrice;
        Text fromAddress;
        Text toAddress;
        Text requestsCount;
        Text date;
        Text time;
    }
}
