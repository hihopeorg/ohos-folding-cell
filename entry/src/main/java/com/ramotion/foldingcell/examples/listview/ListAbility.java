package com.ramotion.foldingcell.examples.listview;

import com.ramotion.foldingcell.FoldingCell;
import com.ramotion.foldingcell.examples.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;

public class ListAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_list);

        // get our list view
        ListContainer theListView = (ListContainer) findComponentById(ResourceTable.Id_mainListView);
        theListView.setBoundaryThickness(AttrHelper.vp2px(10, getContext()));
        ShapeElement boundary = new ShapeElement();
        boundary.setRgbColor(RgbColor.fromArgbInt(0));
        boundary.setBounds(0, 0, 1080, AttrHelper.vp2px(10, getContext()));
        theListView.setBoundary(boundary);
        theListView.setBoundarySwitch(true);

        // prepare elements to display
        final ArrayList<Item> items = Item.getTestingList();

        // add custom btn handler to first list item
        items.get(0).setRequestBtnClickListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                new ToastDialog(getApplicationContext()).setContentCustomComponent(new Text(getContext()) {
                    {
                        setTextSize(14, TextSizeType.FP);
                        setText("CUSTOM HANDLER FOR FIRST BUTTON");
                        setMultipleLine(true);
                        setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                                ComponentContainer.LayoutConfig.MATCH_CONTENT));
                        setPadding(30, 30, 30, 30);
                        setBackground(new ShapeElement() {{
                            setRgbColor(RgbColor.fromArgbInt(0xccffffff));
                            setCornerRadius(60);
                        }});
                    }
                }).setTransparent(true).setOffset(0, 200).setDuration(2000).show();
            }
        });

        // create custom adapter that holds elements and their state (we need hold a id's of unfolded elements for
        // reusable elements)
        final FoldingCellListAdapter adapter = new FoldingCellListAdapter(this, items);

        // add default btn handler for each request btn on each item if custom handler not found
        adapter.setDefaultRequestBtnClickListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                new ToastDialog(getApplicationContext()).setContentCustomComponent(new Text(getContext()) {
                    {
                        setTextSize(14, TextSizeType.FP);
                        setText("DEFAULT HANDLER FOR ALL BUTTONS");
                        setMultipleLine(true);
                        setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                                ComponentContainer.LayoutConfig.MATCH_CONTENT));
                        setPadding(30, 30, 30, 30);
                        setBackground(new ShapeElement() {{
                            setRgbColor(RgbColor.fromArgbInt(0xccffffff));
                            setCornerRadius(60);
                        }});
                    }
                }).setTransparent(true).setOffset(0, 200).setDuration(2000).show();
            }
        });

        // set elements to adapter
        theListView.setItemProvider(adapter);

        // set on click event listener to list view
        theListView.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                // toggle clicked cell state
                boolean toggled = ((FoldingCell) component).toggle(false);
                if (toggled) {
                    // register in adapter that state for selected cell is toggled
                    adapter.registerToggle(i);
                }
            }
        });
    }
}
