package com.ramotion.foldingcell.examples;

import com.ramotion.foldingcell.examples.listview.ListAbility;
import com.ramotion.foldingcell.examples.simple.SimpleAbility;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_btn_1).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder().withBundleName(getBundleName()).withAbilityName(SimpleAbility.class).build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });

        findComponentById(ResourceTable.Id_btn_2).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder().withBundleName(getBundleName()).withAbilityName(ListAbility.class).build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });
    }

}
