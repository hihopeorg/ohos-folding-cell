package com.ramotion.foldingcell.examples;

import com.ramotion.foldingcell.FoldingCell;
import com.ramotion.foldingcell.examples.util.EventHelper;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Button;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.ListContainer;
import ohos.agp.components.StackLayout;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.lang.reflect.Field;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MainAbilityTest {
    private static Ability ability = EventHelper.startAbility(MainAbility.class);
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x01214, "Folding-cellTest");

    private void stopThread(int x) {
        try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void exec(String x) {
        try {
            Runtime.getRuntime().exec(x);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test01instant() {
        Button button = (Button) ability.findComponentById(ResourceTable.Id_btn_1);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        Button button1 = (Button) currentTopAbility.findComponentById(ResourceTable.Id_toggle_instant_btn);
        StackLayout stackLayout = (StackLayout) currentTopAbility.findComponentById(ResourceTable.Id_cell_title_view);
        StackLayout stackLayout1 = (StackLayout) currentTopAbility.findComponentById(ResourceTable.Id_cell_content_view);
        int visibilityA = stackLayout.getVisibility();
        int visibilityB = stackLayout1.getVisibility();
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, button1);
        stopThread(2000);
        int visibilityA1 = stackLayout.getVisibility();
        int visibilityB1 = stackLayout1.getVisibility();
        Assert.assertTrue("卡片未改变", visibilityA != visibilityA1 && visibilityB != visibilityB1);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, button1);
        stopThread(2000);
        exec("input keyevent 4");
        stopThread(1000);
    }

    @Test
    public void test02animation() {
        Button button = (Button) ability.findComponentById(ResourceTable.Id_btn_1);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        Button button1 = (Button) currentTopAbility.findComponentById(ResourceTable.Id_toggle_btn);
        FoldingCell foldingCell = (FoldingCell) currentTopAbility.findComponentById(ResourceTable.Id_folding_cell);
        StackLayout stackLayout = (StackLayout) currentTopAbility.findComponentById(ResourceTable.Id_cell_title_view);
        StackLayout stackLayout1 = (StackLayout) currentTopAbility.findComponentById(ResourceTable.Id_cell_content_view);
        int visibilityA = stackLayout.getVisibility();
        int visibilityB = stackLayout1.getVisibility();
        boolean mAnimationInProgress = false;
        try {
            Field field = FoldingCell.class.getDeclaredField("mAnimationInProgress");
            field.setAccessible(true);
            mAnimationInProgress = (boolean) field.get(foldingCell);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        int childCount = foldingCell.getChildCount();
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, button1);
        stopThread(2000);
        boolean mAnimationInProgress1 = false;
        try {
            Field field = FoldingCell.class.getDeclaredField("mAnimationInProgress");
            field.setAccessible(true);
            mAnimationInProgress1 = (boolean) field.get(foldingCell);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        DirectionalLayout directionalLayout = (DirectionalLayout) foldingCell.getComponentAt(2);
        int childCount1 = foldingCell.getChildCount();
        Assert.assertTrue("动画未进行", childCount != childCount1 && directionalLayout != null && directionalLayout.getChildCount() != 0 && mAnimationInProgress != mAnimationInProgress1);
        stopThread(6000);
        int visibilityA1 = stackLayout.getVisibility();
        int visibilityB1 = stackLayout1.getVisibility();
        Assert.assertTrue("卡片未改变", visibilityA != visibilityA1 && visibilityB != visibilityB1);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, button1);
        stopThread(8000);
        exec("input keyevent 4");
        stopThread(1000);
    }

    @Test
    public void test03animationList() {
        Button button = (Button) ability.findComponentById(ResourceTable.Id_btn_2);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        ListContainer listContainer = (ListContainer) currentTopAbility.findComponentById(ResourceTable.Id_mainListView);
        stopThread(1000);
        FoldingCell foldingCell = (FoldingCell) listContainer.getComponentAt(0);
        DirectionalLayout directionalLayout = (DirectionalLayout) foldingCell.getComponentAt(0);
        DirectionalLayout directionalLayout1 = (DirectionalLayout) foldingCell.getComponentAt(1);
        int visibilityA = directionalLayout.getVisibility();
        int visibilityB = directionalLayout1.getVisibility();
        boolean mAnimationInProgress = false;
        try {
            Field field = FoldingCell.class.getDeclaredField("mAnimationInProgress");
            field.setAccessible(true);
            mAnimationInProgress = (boolean) field.get(foldingCell);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        int childCount = foldingCell.getChildCount();
        exec("input tap 540 555");
        stopThread(800);
        boolean mAnimationInProgress1 = false;
        try {
            Field field = FoldingCell.class.getDeclaredField("mAnimationInProgress");
            field.setAccessible(true);
            mAnimationInProgress1 = (boolean) field.get(foldingCell);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        DirectionalLayout directionalLayout2 = (DirectionalLayout) foldingCell.getComponentAt(2);
        int childCount1 = foldingCell.getChildCount();
        Assert.assertTrue("动画未进行", childCount != childCount1 && directionalLayout2 != null && directionalLayout2.getChildCount() != 0 && mAnimationInProgress != mAnimationInProgress1);
        stopThread(3000);
        int visibilityA1 = directionalLayout.getVisibility();
        int visibilityB1 = directionalLayout1.getVisibility();
        Assert.assertTrue("卡片未改变", visibilityA != visibilityA1 && visibilityB != visibilityB1);
        exec("input tap 540 555");
        stopThread(3000);
        exec("input keyevent 4");
        stopThread(1000);
    }
}