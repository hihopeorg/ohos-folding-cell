package com.ramotion.foldingcell.views;

import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import org.junit.Test;

import static com.ramotion.foldingcell.FoldingCellTest.context;
import static org.junit.Assert.*;

public class FoldingCellViewTest {

    @Test
    public void withFrontElement() {
        FoldingCellView foldingCellView = new FoldingCellView(context,null);
        Element element = new ShapeElement();
        foldingCellView.withFrontElement(element);
        assertEquals(element,foldingCellView.getFrontElement());
    }

    @Test
    public void withBackElement() {
        FoldingCellView foldingCellView = new FoldingCellView(context,null);
        Element element = new ShapeElement();
        foldingCellView.withBackElement(element);
        assertEquals(element,foldingCellView.getmBackElement());
    }

    @Test
    public void getmBackElement() {
        FoldingCellView foldingCellView = new FoldingCellView(context,null);
        Element element = new ShapeElement();
        foldingCellView.withBackElement(element);
        assertEquals(element,foldingCellView.getmBackElement());
    }

    @Test
    public void getFrontElement() {
        FoldingCellView foldingCellView = new FoldingCellView(context,null);
        Element element = new ShapeElement();
        foldingCellView.withFrontElement(element);
        assertEquals(element,foldingCellView.getFrontElement());
    }

    @Test
    public void getFrontHeight() {
        FoldingCellView foldingCellView = new FoldingCellView(context,null);
        Element element = new ShapeElement();
        foldingCellView.withFrontElement(element);
        assertEquals(-1,foldingCellView.getFrontHeight());
    }
}