package com.ramotion.foldingcell;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class FoldingCellTest {
    public static Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();

    @Test
    public void initialize() throws NoSuchFieldException, IllegalAccessException {
        FoldingCell foldingCell = new FoldingCell(context);
        foldingCell.initialize(798,784,7845);
        Field field = FoldingCell.class.getDeclaredField("mAnimationDuration");
        field.setAccessible(true);
        Object fieldValue = field.get(foldingCell);
        assertEquals(798,fieldValue);
    }

    @Test
    public void testInitialize() throws NoSuchFieldException, IllegalAccessException {
        FoldingCell foldingCell = new FoldingCell(context);
        foldingCell.initialize(798,784,7845,8);
        Field field = FoldingCell.class.getDeclaredField("mAnimationDuration");
        field.setAccessible(true);
        Object fieldValue = field.get(foldingCell);
        assertEquals(784,fieldValue);
    }

    @Test
    public void bindItemProvider() throws NoSuchFieldException, IllegalAccessException {
        BaseItemProvider baseItemProvider = new BaseItemProvider() {
            @Override
            public int getCount() {
                return 0;
            }

            @Override
            public Object getItem(int i) {
                return null;
            }

            @Override
            public long getItemId(int i) {
                return 0;
            }

            @Override
            public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
                return null;
            }
        };
        FoldingCell foldingCell = new FoldingCell(context);
        foldingCell.bindItemProvider(baseItemProvider);
        Field field = FoldingCell.class.getDeclaredField("mItemProvider");
        field.setAccessible(true);
        Object fieldValue = field.get(foldingCell);
        assertEquals(baseItemProvider,fieldValue);
    }

    @Test
    public void isUnfolded() {
        FoldingCell foldingCell = new FoldingCell(context);
        assertEquals(false,foldingCell.isUnfolded());
    }
}