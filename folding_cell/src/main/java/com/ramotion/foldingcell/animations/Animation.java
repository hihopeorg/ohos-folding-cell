package com.ramotion.foldingcell.animations;

import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

public abstract class Animation<E extends Component> extends AnimatorValue implements AnimatorValue.ValueUpdateListener {
    protected E target;
    public Animation() {
        setValueUpdateListener(this);
    }

    public void start(E component) {
        target = component;
        start();
    }
}
