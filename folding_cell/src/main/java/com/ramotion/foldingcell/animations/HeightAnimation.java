package com.ramotion.foldingcell.animations;

import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;

/**
 * Height animation for cell container to change cell size according to flip animation.
 */
public class HeightAnimation extends Animation {

    private final Component mView;
    private final int mHeightFrom;
    private final int mHeightTo;
    private BaseItemProvider mItemProvider;

    public HeightAnimation(Component mView, int heightFrom, int heightTo, int duration) {
        this.mView = mView;
        this.mHeightFrom = heightFrom;
        this.mHeightTo = heightTo;
        this.setDuration(duration);
    }

    public HeightAnimation withItemProvider(BaseItemProvider itemProvider) {
        this.mItemProvider = itemProvider;
        return this;
    }

    public HeightAnimation withInterpolator(TimelineCurve interpolator) {
        if (interpolator != null) {
            this.setCurve(interpolator);
        }
        return this;
    }

    @Override
    public void onUpdate(AnimatorValue animatorValue, float v) {
        float newHeight = mHeightFrom + (mHeightTo - mHeightFrom) * v;

        if (v == 1) {
            mView.getLayoutConfig().height = mHeightTo;
        } else {
            mView.getLayoutConfig().height = (int) newHeight;
        }
        mView.setLayoutConfig(mView.getLayoutConfig());
        if (mItemProvider != null) {
            mItemProvider.notifyDataChanged();
        }
    }

    @Override
    public String toString() {
        return "HeightAnimation{" +
                "mHeightFrom=" + mHeightFrom +
                ", mHeightTo=" + mHeightTo +
                ", duration =" + getDuration() +
                '}';
    }
}
