package com.ramotion.foldingcell.animations;

import ohos.agp.animation.Animator;

/**
 * Just sugar for code clean
 */
public abstract class AnimationEndListener implements Animator.StateChangedListener, Animator.LoopedListener {
    @Override
    public void onStart(Animator animator) {
        // do nothing
    }

    @Override
    public void onStop(Animator animator) {
        // do nothing
    }

    @Override
    public void onCancel(Animator animator) {
        // do nothing
    }

    @Override
    public void onPause(Animator animator) {
        // do nothing
    }

    @Override
    public void onResume(Animator animator) {
        // do nothing
    }

    @Override
    public void onRepeat(Animator animator) {
        // do nothing
    }
}
