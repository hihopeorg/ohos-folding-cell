package com.ramotion.foldingcell.animations;

import com.ramotion.foldingcell.FoldingCell;
import com.ramotion.foldingcell.views.FoldingCellView;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.render.ThreeDimView;

/**
 * Main piece of fold animation. Rotates view in 3d space around one of view borders.
 */
@SuppressWarnings("unused")
public class FoldAnimation extends Animation<FoldingCellView> {
    private static final int MIN_PX = 1;
    private static final float DEFAULT_CAMERA_HEIGHT = 8f;

    public enum FoldAnimationMode {
        FOLD_UP, UNFOLD_DOWN, FOLD_DOWN, UNFOLD_UP
    }

    private final FoldingCell mFoldingCell;
    private final FoldAnimationMode mFoldMode;
    private final int mCameraHeight;
    private float mFromDegrees;
    private float mToDegrees;
    private float mCurDegrees;
    private int mCenterX;
    private int mCenterY;
    private float scale = 1;

    private Component.DrawTask drawTask = new Component.DrawTask() {
        @Override
        public void onDraw(Component component, Canvas canvas) {
            int frontTop = 0;

            Element backElement = target.getmBackElement();
            Element frontElement = target.getFrontElement();

            if (backElement != null) {
                backElement.setBounds(0, 0,
                       component.getWidth(),
                        component.getHeight());
            }
            if (frontElement != null) {
                frontTop = component.getHeight() - target.getFrontHeight();
                frontElement.setBounds(0, frontTop,
                        component.getWidth() ,
                        component.getHeight());
            }

            ThreeDimView mCamera = new ThreeDimView();
            if (mCurDegrees <= 0) {
                if (backElement != null) {
                    backElement.drawToCanvas(canvas);
                }
            }
            canvas.translate(mCenterX, mCenterY);
            canvas.scale(scale, scale);
            mCamera.rotateX(mCurDegrees);
            mCamera.applyToCanvas(canvas);
            canvas.scale(1 / scale, 1 / scale);
            canvas.translate(-mCenterX, -mCenterY);
            if (backElement != null && mCurDegrees > 0 && needToDraw(backElement, mCurDegrees)) {
                backElement.drawToCanvas(canvas);
            }
            if (frontElement != null && needToDraw(frontElement, mCurDegrees)) {
                frontElement.drawToCanvas(canvas);
            }
        }
    };

    private boolean needToDraw(Element element, float degree) {
        return element.getHeight() * Math.cos(Math.toRadians(degree)) >= MIN_PX;
    }

    public FoldAnimation(FoldingCell foldingCell, FoldAnimationMode foldMode, int cameraHeight, long duration) {
        this.mFoldingCell = foldingCell;
        this.mFoldMode = foldMode;
        this.setDuration(duration);
        this.mCameraHeight = cameraHeight;
        // For ohos ThreeDimView has no z-location option , we use a scale to emulate z-axis depth of camera.
        this.scale = cameraHeight / DEFAULT_CAMERA_HEIGHT;
        super.setStateChangedListener(new AnimationEndListener() {
            @Override
            public void onEnd(Animator animator) {
                if (userStateChangedListener!=null) {
                    userStateChangedListener.onEnd(animator);
                }
            }
        });
    }

    public FoldAnimation withAnimationListener(StateChangedListener animationListener) {
        this.setStateChangedListener(animationListener);
        return this;
    }

    public FoldAnimation withStartOffset(int offset) {
        this.setDelay(offset);
        return this;
    }

    public FoldAnimation withInterpolator(TimelineCurve interpolator) {
        if (interpolator != null) {
            this.setCurve(interpolator);
        }
        return this;
    }

    @Override
    public void start(FoldingCellView foldingCell) {
        super.start(foldingCell);
    }

    @Override
    public void start() {
        if (target != null) {
            Component parent = (Component) target.getComponentParent();
            initialize(target.getWidth(), target.getHeight(), parent.getWidth(), parent.getHeight());
        }
        super.start();
        if (mFoldMode == FoldAnimationMode.UNFOLD_UP||mFoldMode==FoldAnimationMode.UNFOLD_DOWN) {
            onUpdate(this, 0);
        }
    }

    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        this.mCenterX = width / 2;
        switch (mFoldMode) {
            case FOLD_UP:
                this.mCenterY = 0;
                this.mFromDegrees = 0;
                this.mToDegrees = 90;
                break;
            case FOLD_DOWN:
                this.mCenterY = height;
                this.mFromDegrees = 0;
                this.mToDegrees = -90;
                break;
            case UNFOLD_UP:
                this.mCenterY = height;
                this.mFromDegrees = -90;
                this.mToDegrees = 0;
                break;
            case UNFOLD_DOWN:
                this.mCenterY = 0;
                this.mFromDegrees = 90;
                this.mToDegrees = 0;
                break;
            default:
                throw new IllegalStateException("Unknown animation mode.");
        }
        mCurDegrees = mFromDegrees;
    }

    @Override
    public void onUpdate(AnimatorValue animatorValue, float v) {
        if (v == 0 && (mFoldMode == FoldAnimationMode.FOLD_DOWN || mFoldMode == FoldAnimationMode.UNFOLD_DOWN)) {
            return;
        }
        final float fromDegrees = mFromDegrees;
        mCurDegrees = fromDegrees + ((mToDegrees - fromDegrees) * v);
        target.addDrawTask(drawTask);
        if (mFoldingCell.getComponentParent() != null) {
            ((Component)(mFoldingCell.getComponentParent())).invalidate();
        }
    }

    private StateChangedListener userStateChangedListener;

    @Override
    public void setStateChangedListener(StateChangedListener listener) {
        userStateChangedListener = listener;
    }

    @Override
    public String toString() {
        return "FoldAnimation{" + "mFoldMode=" + mFoldMode + ", mFromDegrees=" + mFromDegrees + ", mToDegrees=" + mToDegrees + '}';
    }

}
