package com.ramotion.foldingcell;

import com.ramotion.foldingcell.animations.Animation;
import com.ramotion.foldingcell.animations.AnimationEndListener;
import com.ramotion.foldingcell.animations.FoldAnimation;
import com.ramotion.foldingcell.animations.HeightAnimation;
import com.ramotion.foldingcell.views.FoldingCellView;
import ohos.agp.animation.Animator;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Very first implementation of Folding Cell by Ramotion for ohos platform
 */
public class FoldingCell extends DependentLayout {

    // state variables
    private boolean mUnfolded;
    private boolean mAnimationInProgress;

    // default values
    private final int DEF_ANIMATION_DURATION = 1000;
    private final int DEF_BACK_SIDE_COLOR = Color.GRAY.getValue();
    private final int DEF_ADDITIONAL_FLIPS = 0;
    private final int DEF_CAMERA_HEIGHT = 30;

    // current settings
    private int mAnimationDuration = DEF_ANIMATION_DURATION;
    private int mBackSideColor = DEF_BACK_SIDE_COLOR;
    private int mAdditionalFlipsCount = DEF_ADDITIONAL_FLIPS;
    private int mCameraHeight = DEF_CAMERA_HEIGHT;

    // In ListContainer scene, as item view's getComponentParent returns null, use ItemProvider to notify parent refreshing.
    private BaseItemProvider mItemProvider;

    public FoldingCell(Context context) {
        this(context, null);
    }

    public FoldingCell(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public FoldingCell(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (attrs != null) {
            final int count = attrs.getLength();
            for (int i = 0; i < count; ++i) {
                Attr attr = attrs.getAttr(i).get();
                switch (attr.getName()) {
                    case "animationDuration":
                        this.mAnimationDuration = attr.getIntegerValue();
                        break;
                    case "backSideColor":
                        this.mBackSideColor = attr.getColorValue().getValue();
                        break;
                    case "additionalFlipsCount":
                        this.mAdditionalFlipsCount = attr.getIntegerValue();
                        break;
                    case "cameraHeight":
                        this.mCameraHeight = attr.getIntegerValue();
                        break;
                    default:
                        break;
                }
            }
        }
    }

    /**
     * Initializes folding cell programmatically with custom settings
     *
     * @param animationDuration    animation duration, default is 1000
     * @param backSideColor        color of back side, default is ohos.agp.utils.Color.GRAY (0xFF888888)
     * @param additionalFlipsCount count of additional flips (after first one), set 0 for auto
     */
    public void initialize(int animationDuration, int backSideColor, int additionalFlipsCount) {
        this.mAnimationDuration = animationDuration;
        this.mBackSideColor = backSideColor;
        this.mAdditionalFlipsCount = additionalFlipsCount;
    }

    /**
     * Initializes folding cell programmatically with custom settings
     *
     * @param animationDuration    animation duration, default is 1000
     * @param backSideColor        color of back side, default is ohos.agp.utils.Color.GRAY (0xFF888888)
     * @param additionalFlipsCount count of additional flips (after first one), set 0 for auto
     */
    public void initialize(int cameraHeight, int animationDuration, int backSideColor, int additionalFlipsCount) {
        this.mAnimationDuration = animationDuration;
        this.mBackSideColor = backSideColor;
        this.mAdditionalFlipsCount = additionalFlipsCount;
        this.mCameraHeight = cameraHeight;
    }

    public void bindItemProvider(BaseItemProvider itemProvider) {
        this.mItemProvider = itemProvider;
    }
    public boolean isUnfolded() {
        return mUnfolded;
    }

    /**
     * Unfold cell with (or without) animation
     *
     * @param skipAnimation if true - change state of cell instantly without animation
     */
    public boolean unfold(boolean skipAnimation) {
        if (mUnfolded || mAnimationInProgress) return false;

        // get main content parts
        final Component contentView = getComponentAt(0);
        if (contentView == null) return false;
        final Component titleView = getComponentAt(1);
        if (titleView == null) return false;

        // hide title and content views
        titleView.setVisibility(HIDE);
        contentView.setVisibility(HIDE);
        if (skipAnimation) {
            contentView.setVisibility(VISIBLE);
            FoldingCell.this.mUnfolded = true;
            FoldingCell.this.mAnimationInProgress = false;
            this.getLayoutConfig().height = contentView.getHeight();
            this.setLayoutConfig(this.getLayoutConfig());
        } else {
            // Measure views and take a bitmaps to replace real views with images
            PixelMap bitmapFromTitleView = measureViewAndGetBitmap(titleView, this.getEstimatedWidth());
            PixelMap bitmapFromContentView = measureViewAndGetBitmap(contentView, this.getEstimatedWidth());
            // create layout container for animation elements
            final DirectionalLayout foldingLayout = createAndPrepareFoldingContainer();
            this.addComponent(foldingLayout);
            // calculate heights of animation parts
            ArrayList<Integer> heights = calculateHeightsForAnimationParts(titleView.getHeight(),
                    contentView.getHeight(), mAdditionalFlipsCount);
            // create list with animation parts for animation
            ArrayList<FoldingCellView> foldingCellElements = prepareViewsForAnimation(heights, bitmapFromTitleView,
                    bitmapFromContentView);
            // start unfold animation with end listener
            int childCount = foldingCellElements.size();
            int part90degreeAnimationDuration = mAnimationDuration / (childCount * 2);
            startUnfoldAnimation(foldingCellElements, foldingLayout, part90degreeAnimationDuration,
                    new AnimationEndListener() {
                public void onEnd(Animator animation) {
                    contentView.setVisibility(VISIBLE);
                    foldingLayout.setVisibility(HIDE);
                    FoldingCell.this.removeComponent(foldingLayout);
                    FoldingCell.this.mUnfolded = true;
                    FoldingCell.this.mAnimationInProgress = false;
                }
            });
            startExpandHeightAnimation(heights, part90degreeAnimationDuration * 2);
            this.mAnimationInProgress = true;
        }
        return true;
    }

    /**
     * Fold cell with (or without) animation
     *
     * @param skipAnimation if true - change state of cell instantly without animation
     */
    public boolean fold(boolean skipAnimation) {
        if (!mUnfolded || mAnimationInProgress) return false;

        // get basic views
        final Component contentView = getComponentAt(0);
        if (contentView == null) return false;
        final Component titleView = getComponentAt(1);
        if (titleView == null) return false;

        // hide title and content views
        titleView.setVisibility(HIDE);
        contentView.setVisibility(HIDE);

        if (skipAnimation) {
            contentView.setVisibility(HIDE);
            titleView.setVisibility(VISIBLE);
            FoldingCell.this.mAnimationInProgress = false;
            FoldingCell.this.mUnfolded = false;
            this.getLayoutConfig().height = titleView.getHeight();
            this.setLayoutConfig(this.getLayoutConfig());
        } else {
            // make bitmaps from title and content views
            PixelMap bitmapFromTitleView = measureViewAndGetBitmap(titleView, this.getEstimatedWidth());
            PixelMap bitmapFromContentView = measureViewAndGetBitmap(contentView, this.getEstimatedWidth());
            //ViewCompat.setHasTransientState(this, true);
            // create empty layout for folding animation
            final DirectionalLayout foldingLayout = createAndPrepareFoldingContainer();//创建一个纵向的DirectionalLayout
            // add that layout to structure
            this.addComponent(foldingLayout);

            // calculate heights of animation parts
            ArrayList<Integer> heights = calculateHeightsForAnimationParts(titleView.getHeight(),
                    contentView.getHeight(), mAdditionalFlipsCount);//获取高度数组 为折叠区间 [titleHeight, titleHeight, x, ..., x, rest]
            // create list with animation parts for animation
            ArrayList<FoldingCellView> foldingCellElements = prepareViewsForAnimation(heights, bitmapFromTitleView,
                    bitmapFromContentView);//为每次折叠动作创建一个FoldingCellView对象
            int childCount = foldingCellElements.size();
            int part90degreeAnimationDuration = mAnimationDuration / (childCount * 2);
            // start fold animation with end listener
            startFoldAnimation(foldingCellElements, foldingLayout, part90degreeAnimationDuration,
                    new AnimationEndListener() {
                @Override
                public void onEnd(Animator animation) {
                    contentView.setVisibility(HIDE);
                    titleView.setVisibility(VISIBLE);
                    foldingLayout.setVisibility(HIDE);
                    FoldingCell.this.removeComponent(foldingLayout);
                    FoldingCell.this.mAnimationInProgress = false;
                    FoldingCell.this.mUnfolded = false;
                    //ViewCompat.setHasTransientState(FoldingCell.this, true);
                }
            });
            startCollapseHeightAnimation(heights, part90degreeAnimationDuration * 2);
            this.mAnimationInProgress = true;
        }
        return true;
    }

    /**
     * Toggle current state of FoldingCellLayout
     */
    public boolean toggle(boolean skipAnimation) {
        boolean result = false;
        if (this.mUnfolded) {
            result = this.fold(skipAnimation);
        } else {
            result = this.unfold(skipAnimation);
        }
        this.postLayout();
        return result;
    }

    /**
     * Create and prepare list of FoldingCellViews with different bitmap parts for fold animation
     *
     * @param titleViewBitmap   bitmap from title view
     * @param contentViewBitmap bitmap from content view
     * @return list of FoldingCellViews with bitmap parts
     */
    protected ArrayList<FoldingCellView> prepareViewsForAnimation(ArrayList<Integer> viewHeights,
                                                                  PixelMap titleViewBitmap,
                                                                  PixelMap contentViewBitmap) {
        if (viewHeights == null || viewHeights.isEmpty())
            throw new IllegalStateException("ViewHeights array must be not null and not empty");

        ArrayList<FoldingCellView> partsList = new ArrayList<>();

        int partWidth = titleViewBitmap.getImageInfo().size.width;
        int yOffset = 0;
        for (int i = 0; i < viewHeights.size(); i++) {
            int partHeight = viewHeights.get(i);
            PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
            initializationOptions.size = new Size(partWidth, partHeight);
            initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
            initializationOptions.editable = true;
            PixelMap partBitmap = PixelMap.create(initializationOptions);
            Canvas canvas = new Canvas(new Texture(partBitmap));
            RectFloat srcRect = new RectFloat(0, yOffset, partWidth, yOffset + partHeight);
            RectFloat destRect = new RectFloat(0, 0, partWidth, partHeight);
            canvas.drawPixelMapHolderRect(new PixelMapHolder(contentViewBitmap), srcRect, destRect, new Paint());
            Element backElement = createImageElementFromBitmap(partBitmap);
            Element frontElement = null;
            if (i < viewHeights.size() - 1) {
                frontElement = (i == 0) ? createImageElementFromBitmap(titleViewBitmap) ://第一次翻转动作 前台图为title content内容
                        createBackSideElement(partWidth, viewHeights.get(i + 1), titleViewBitmap.getImageInfo().size.height);//非第一次翻转动作 前台图用灰图
            }
            partsList.add(new FoldingCellView(frontElement, backElement, getContext()));
            yOffset = yOffset + partHeight;
        }

        return partsList;
    }

    /**
     * Calculate heights for animation parts with some logic
     *
     * @param titleViewHeight      height of title view
     * @param contentViewHeight    height of content view
     * @param additionalFlipsCount count of additional flips (after first one), set 0 for auto
     * @return list of calculated heights
     */
    protected ArrayList<Integer> calculateHeightsForAnimationParts(int titleViewHeight, int contentViewHeight,
                                                                   int additionalFlipsCount) {
        ArrayList<Integer> partHeights = new ArrayList<>();
        int additionalPartsTotalHeight = contentViewHeight - titleViewHeight * 2;
        if (additionalPartsTotalHeight < 0) throw new IllegalStateException("Content View height is too small");
        // add two main parts - guarantee first flip
        partHeights.add(titleViewHeight);
        partHeights.add(titleViewHeight);

        // if no space left - return
        if (additionalPartsTotalHeight == 0) return partHeights;

        // if some space remained - use two different logic
        if (additionalFlipsCount != 0) {
            // 1 - additional parts count is specified and it is not 0 - divide remained space
            int additionalPartHeight = additionalPartsTotalHeight / additionalFlipsCount;
            int remainingHeight = additionalPartsTotalHeight % additionalFlipsCount;

            if (additionalPartHeight + remainingHeight > titleViewHeight)
                throw new IllegalStateException("Additional flips count is too small");
            for (int i = 0; i < additionalFlipsCount; i++)
                partHeights.add(additionalPartHeight + (i == 0 ? remainingHeight : 0));
        } else {
            // 2 - additional parts count isn't specified or 0 - divide remained space to parts with title view size
            int partsCount = additionalPartsTotalHeight / titleViewHeight;
            int restPartHeight = additionalPartsTotalHeight % titleViewHeight;
            for (int i = 0; i < partsCount; i++)
                partHeights.add(titleViewHeight);
            if (restPartHeight > 0) partHeights.add(restPartHeight);
        }

        return partHeights;
    }

    /**
     * Create image view for display back side of flip view
     *
     * @param width width for view
     * @param height height for view
     * @return Image with selected height and default background color
     */
    protected Element createBackSideElement(int width, int height, int titleHeight) {
        // On some device/api (such as P40Pro SDK7 emulator), Element.setBounds() has no effect.
        // So we create a full size Element, use transparent color to fill out-bounds area.
        if (height < titleHeight) {
            PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
            initializationOptions.size = new Size(width, titleHeight);
            initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
            initializationOptions.editable = true;
            PixelMap bitmap = PixelMap.create(initializationOptions);
            Canvas canvas = new Canvas(new Texture(bitmap));
            Paint paint = new Paint();
            paint.setColor(new Color(mBackSideColor));
            canvas.drawRect(new Rect(0, titleHeight - height, width, titleHeight), paint);
            return createImageElementFromBitmap(bitmap);
        } else {
            ShapeElement background = new ShapeElement();
            background.setRgbColor(RgbColor.fromArgbInt(mBackSideColor));
            background.setBounds(0, 0, width, height);
            return background;
        }
    }

    /**
     * Create image view for display selected bitmap
     *
     * @param bitmap bitmap to display in image view
     * @return Image with selected bitmap
     */
    protected Element createImageElementFromBitmap(PixelMap bitmap) {
        return new PixelMapElement(bitmap);
    }

    /**
     * Create bitmap from specified View with specified with
     *
     * @param view        source for bitmap
     * @param parentWidth result bitmap width
     * @return bitmap from specified view
     */
    protected PixelMap measureViewAndGetBitmap(Component view, int parentWidth) {
        int specW = EstimateSpec.getSizeWithMode(parentWidth, EstimateSpec.PRECISE);
        int specH = EstimateSpec.getSizeWithMode(0, EstimateSpec.UNCONSTRAINT);
        view.estimateSize(specW, specH);
        view.arrange(0, 0, view.getEstimatedWidth(), view.getEstimatedHeight());
        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        initializationOptions.size = new Size(view.getWidth(), view.getHeight());
        initializationOptions.editable = true;
        PixelMap b = PixelMap.create(initializationOptions);
        Canvas c = new Canvas(new Texture(b));
        c.translate(-view.getScrollValue(AXIS_X), -view.getScrollValue(AXIS_Y));
        drawComponentToCanvas(view, c);
        return b;
    }

    // not support dump content layer
    private void drawComponentToCanvas(Component component, Canvas canvas) {
        if (component == null || canvas == null) {
            return;
        }
        Element background = component.getBackgroundElement();
        if (background != null) {
            background.setBounds(0, 0, component.getWidth(), component.getHeight());
            drawElementToCanvas(background, canvas);
        }

        try {
            Field mDrawTaskUnderContentField = Component.class.getDeclaredField("mDrawTaskUnderContent");
            mDrawTaskUnderContentField.setAccessible(true);
            DrawTask mDrawTaskUnderContent = (DrawTask) mDrawTaskUnderContentField.get(component);
            if (mDrawTaskUnderContent != null) {
                mDrawTaskUnderContent.onDraw(component, canvas);
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        if (component instanceof ComponentContainer) {
            int childCount = ((ComponentContainer) component).getChildCount();
            for (int i = 0; i < childCount; i++) {
                int saveCount = canvas.save();
                Component child = ((ComponentContainer) component).getComponentAt(i);
                canvas.translate(component.getPaddingLeft() + child.getContentPositionX(),
                        component.getPaddingTop() + child.getContentPositionY());
                drawComponentToCanvas(child, canvas);
                canvas.restoreToCount(saveCount);
            }
        }
        try {
            Field mDrawTaskOverContentField = Component.class.getDeclaredField("mDrawTaskOverContent");
            mDrawTaskOverContentField.setAccessible(true);
            DrawTask mDrawTaskOverContent = (DrawTask) mDrawTaskOverContentField.get(component);
            if (mDrawTaskOverContent != null) {
                mDrawTaskOverContent.onDraw(component, canvas);
            }

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        Element foreground = component.getForegroundElement();
        if (foreground != null) {
            foreground.setBounds(0, 0, component.getWidth(), component.getHeight());
            drawElementToCanvas(foreground, canvas);
        }
    }

    // Workaround, for Element.drawToCanvas(Canvas) can not draw to Canvas's off-screen texture so far.
    private void drawElementToCanvas(Element element, Canvas canvas) {
        if (element == null || canvas == null) {
            return;
        }
        if (element instanceof ShapeElement) {
            ShapeElement shapeElement = (ShapeElement) element;
            RgbColor[] colors = shapeElement.getRgbColors();
            if (colors != null && colors.length != 0) {
                Paint paint = new Paint();
                paint.setColor(new Color(colors[0].asArgbInt()));
                canvas.drawRect(element.getBounds(), paint);
            }
        } else if (element instanceof PixelMapElement) {
            PixelMap pixelMap = ((PixelMapElement) element).getPixelMap();
            if (pixelMap != null) {
                canvas.drawPixelMapHolderRect(new PixelMapHolder(pixelMap), new RectFloat(element.getBounds()),
                        new Paint());
            }
        }
    }

    /**
     * Create layout that will be a container for animation elements
     *
     * @return Configured container for animation elements (DirectionalLayout)
     */
    protected DirectionalLayout createAndPrepareFoldingContainer() {
        DirectionalLayout foldingContainer = new DirectionalLayout(getContext()){
            @Override
            public void addComponent(Component childComponent) {
                childComponent.setClipEnabled(false);
                super.addComponent(childComponent);
            }
        };

        foldingContainer.setOrientation(DirectionalLayout.VERTICAL);
        foldingContainer.setLayoutConfig(new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                LayoutConfig.MATCH_CONTENT));
        return foldingContainer;
    }

    /**
     * Prepare and start height expand animation for FoldingCellLayout
     *
     * @param partAnimationDuration one part animate duration
     * @param viewHeights           heights of animation parts
     */
    protected void startExpandHeightAnimation(ArrayList<Integer> viewHeights, int partAnimationDuration) {
        if (viewHeights == null || viewHeights.isEmpty())
            throw new IllegalArgumentException("ViewHeights array must have at least 2 elements");

        ArrayList<Animation> heightAnimations = new ArrayList<>();
        int fromHeight = viewHeights.get(0);
        int delay = 0;
        int animationDuration = partAnimationDuration - delay;
        for (int i = 1; i < viewHeights.size(); i++) {
            int toHeight = fromHeight + viewHeights.get(i);
            HeightAnimation heightAnimation =
                    new HeightAnimation(this, fromHeight, toHeight, animationDuration)
                            .withInterpolator(new DecelerateInterpolator()).withItemProvider(mItemProvider);
            heightAnimation.setDelay(delay);
            heightAnimations.add(heightAnimation);
            fromHeight = toHeight;
        }
        createAnimationChain(heightAnimations, this);
        heightAnimations.get(0).start(this);
        int finalHeight = fromHeight;
        getContext().getUITaskDispatcher().delayDispatch(() -> {
            // in case animation not start successfully
            heightAnimations.get(heightAnimations.size()-1).onUpdate(null, 1);
        }, mAnimationDuration);
    }

    /**
     * Prepare and start height collapse animation for FoldingCellLayout
     *
     * @param partAnimationDuration one part animate duration
     * @param viewHeights           heights of animation parts
     */
    protected void startCollapseHeightAnimation(ArrayList<Integer> viewHeights, int partAnimationDuration) {
        if (viewHeights == null || viewHeights.isEmpty())
            throw new IllegalArgumentException("ViewHeights array must have at least 2 elements");

        ArrayList<Animation> heightAnimations = new ArrayList<>();
        int fromHeight = viewHeights.get(0);
        for (int i = 1; i < viewHeights.size(); i++) {
            int toHeight = fromHeight + viewHeights.get(i);
            heightAnimations.add(new HeightAnimation(this, toHeight, fromHeight, partAnimationDuration)
                    .withInterpolator(new DecelerateInterpolator()).withItemProvider(mItemProvider));
            fromHeight = toHeight;
        }

        Collections.reverse(heightAnimations);
        createAnimationChain(heightAnimations, this);
        heightAnimations.get(0).start(this);
        int finalHeight = fromHeight;
        getContext().getUITaskDispatcher().delayDispatch(() -> {
            // in case animation not start successfully
            heightAnimations.get(heightAnimations.size()-1).onUpdate(null, 1);
        }, mAnimationDuration);
    }

    /**
     * Create "animation chain" for selected view from list of animation objects
     *
     * @param animationList   collection with animations
     * @param animationObject view for animations
     */
    protected void createAnimationChain(final List<Animation> animationList, final Component animationObject) {
        for (int i = 0; i < animationList.size(); i++) {
            Animation animation = animationList.get(i);
            if (i + 1 < animationList.size()) {
                final int finalI = i;
                animation.setStateChangedListener(new AnimationEndListener() {
                    @Override
                    public void onEnd(Animator animation) {
                        animationList.get(finalI + 1).start(animationObject);
                    }
                });
            }
        }
    }

    /**
     * Start fold animation
     *
     * @param foldingCellElements           ordered list with animation parts from top to bottom
     * @param foldingLayout                 prepared layout for animation parts
     * @param part90degreeAnimationDuration animation duration for 90 degree rotation
     * @param animationEndListener          animation end callback
     */
    protected void startFoldAnimation(ArrayList<FoldingCellView> foldingCellElements,
                                      ComponentContainer foldingLayout, int part90degreeAnimationDuration,
                                      AnimationEndListener animationEndListener) {
        for (FoldingCellView foldingCellElement : foldingCellElements)
            foldingLayout.addComponent(foldingCellElement);

        Collections.reverse(foldingCellElements);

        int nextDelay = 0;
        for (int i = 0; i < foldingCellElements.size(); i++) {
            FoldingCellView cell = foldingCellElements.get(i);
            cell.setVisibility(VISIBLE);
            // not FIRST(BOTTOM) element - animate front view
            if (i != 0) {
                FoldAnimation foldAnimation = new FoldAnimation(this, FoldAnimation.FoldAnimationMode.UNFOLD_UP,
                        mCameraHeight, part90degreeAnimationDuration).withStartOffset(nextDelay).withInterpolator(new DecelerateInterpolator());
                // if last(top) element - add end listener
                if (i == foldingCellElements.size() - 1) {
                    foldAnimation.setStateChangedListener(animationEndListener);
                }
                cell.animateFrontView(foldAnimation);
                nextDelay = nextDelay + part90degreeAnimationDuration;
                // in case animation not start successfully
                if (i == foldingCellElements.size() - 1) {
                    getContext().getUITaskDispatcher().delayDispatch(() -> {
                        animationEndListener.onEnd(null);
                    }, nextDelay);
                }
            }
            // if not last(top) element - animate whole view
            if (i != foldingCellElements.size() - 1) {
                new FoldAnimation(this, FoldAnimation.FoldAnimationMode.FOLD_UP, mCameraHeight,
                        part90degreeAnimationDuration).withStartOffset(nextDelay).withInterpolator(new DecelerateInterpolator()).start(cell);
                nextDelay = nextDelay + part90degreeAnimationDuration;
            }
        }
    }

    /**
     * Start unfold animation
     *
     * @param foldingCellElements           ordered list with animation parts from top to bottom
     * @param foldingLayout                 prepared layout for animation parts
     * @param part90degreeAnimationDuration animation duration for 90 degree rotation
     * @param animationEndListener          animation end callback
     */
    protected void startUnfoldAnimation(ArrayList<FoldingCellView> foldingCellElements,
                                        ComponentContainer foldingLayout, int part90degreeAnimationDuration,
                                        AnimationEndListener animationEndListener) {
        int nextDelay = 0;
        for (int i = 0; i < foldingCellElements.size(); i++) {
            FoldingCellView cell = foldingCellElements.get(i);
            cell.setVisibility(VISIBLE);
            foldingLayout.addComponent(cell);
            // if not first(top) element - animate whole view
            if (i != 0) {
                FoldAnimation foldAnimation = new FoldAnimation(this, FoldAnimation.FoldAnimationMode.UNFOLD_DOWN,
                        mCameraHeight, part90degreeAnimationDuration).withStartOffset(nextDelay).withInterpolator(new DecelerateInterpolator());
                // if last(bottom) element - add end listener
                if (i == foldingCellElements.size() - 1) {
                    foldAnimation.setStateChangedListener(animationEndListener);
                }

                nextDelay = nextDelay + part90degreeAnimationDuration;
                // in case animation not start successfully
                if (i == foldingCellElements.size() - 1) {
                    getContext().getUITaskDispatcher().delayDispatch(() -> {
                        animationEndListener.onEnd(null);
                    }, nextDelay);
                }
                foldAnimation.start(cell);
            }
            // not last(bottom) element - animate front view
            if (i != foldingCellElements.size() - 1) {
                cell.animateFrontView(new FoldAnimation(this, FoldAnimation.FoldAnimationMode.FOLD_DOWN, mCameraHeight,
                        part90degreeAnimationDuration).withStartOffset(nextDelay).withInterpolator(new DecelerateInterpolator()));
                nextDelay = nextDelay + part90degreeAnimationDuration;
            }
        }
    }

    private class DecelerateInterpolator implements Animator.TimelineCurve {

        @Override
        public float getCurvedTime(float v) {
            return 1.0f - (1.0f - v) * (1.0f - v);
        }
    }
}
