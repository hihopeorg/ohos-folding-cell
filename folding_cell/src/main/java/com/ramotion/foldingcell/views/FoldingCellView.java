package com.ramotion.foldingcell.views;

import com.ramotion.foldingcell.animations.Animation;
import ohos.agp.components.AttrSet;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.element.Element;
import ohos.app.Context;

/**
 * Basic element for folding animation that represents one physic part of folding sheet with different views on front
 * and back.
 */
@SuppressWarnings("unused")
public class FoldingCellView extends DependentLayout {
    private Element mBackElement;
    private Element mFrontElement;
    private int mFrontHeight = 0;

    public FoldingCellView(Context context, AttrSet attrs) {
        super(context, attrs);
        LayoutConfig layoutParams = new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_CONTENT);
        this.setLayoutConfig(layoutParams);
    }

    public FoldingCellView(Element frontElement, Element backElement, Context context) {
        super(context);
        this.mFrontElement = frontElement;
        this.mBackElement = backElement;

        LayoutConfig layoutParams = new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_CONTENT);


        if (mBackElement != null) {
            layoutParams.height = mBackElement.getHeight();
            layoutParams.width = mBackElement.getWidth();
        }

        if (mFrontElement != null) {
            layoutParams.height = Math.max(layoutParams.height, mFrontElement.getHeight());
            layoutParams.width = Math.max(layoutParams.width, mFrontElement.getWidth());
        }
        resetFrontElementOffset();
        this.setLayoutConfig(layoutParams);
    }

    public FoldingCellView withFrontElement(Element frontElement) {
        this.mFrontElement = frontElement;
        resetFrontElementOffset();
        return this;
    }

    public FoldingCellView withBackElement(Element backElement) {
        this.mBackElement = backElement;

        if (mBackElement != null) {
            LayoutConfig layoutParams = (LayoutConfig) this.getLayoutConfig();
            layoutParams.height = mBackElement.getHeight();
            layoutParams.width = mBackElement.getWidth();
            this.setLayoutConfig(layoutParams);
        }
        resetFrontElementOffset();
        return this;
    }

    public Element getmBackElement() {
        return mBackElement;
    }

    public Element getFrontElement() {
        return mFrontElement;
    }

    private void resetFrontElementOffset() {
        mFrontHeight = 0;
        if (mFrontElement != null) {
            int componentHeight = this.getHeight();
            mFrontHeight = mFrontElement.getHeight();
            mFrontElement.setBounds(0, componentHeight - mFrontHeight, mFrontElement.getWidth(), componentHeight);
        }
    }

    public int getFrontHeight() {
        return mFrontHeight;
    }

    public void animateFrontView(Animation animation) {
        animation.start(this);
        //if (this.mFrontView != null) animation.start(mFrontView);
    }

}
